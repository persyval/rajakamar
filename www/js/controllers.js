angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout) {
        // Form data for the login modal
        $scope.loginData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function () {
            $scope.modal.show();
        };

        $scope.showDesktopSite = function () {
            window.open('http://rajakamar.com/', '_system', 'location=yes');
        }
        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            console.log('Doing login', $scope.loginData);

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $timeout(function () {
                $scope.closeLogin();
            }, 500);
        };
    })

    .controller('PlaylistsCtrl', function ($scope, $filter) {

        var self = this;

        self.location = {};
        self.location.locationName = "Jakarta";
        self.date = {};
        self.date.checkin = "6 mar 15";
        self.date.checkout = "7 mar 15";
        self.total = {};
        self.total.kamar = 1;

        self.clear = function() {
            self.location.locationName = "";
        };

        self.hideKeyboard = function() {
            Keyboard.close();
        };

        self.add = function() {
            self.total.kamar ++;
        };

        self.substract = function() {
            if (self.total.kamar > 0)
            {
                self.total.kamar --;
            }
        };

        self.pickDateCheckin = function() {

            var self = this;

            var options = {
                date: new Date(),
                mode: 'date'
            };

            datePicker.show(options, function(date){
                var appDate = $filter('date')(date, "d MMM yy");
                self.date.checkin = appDate;
                $scope.$apply();
            });
        }

        self.pickDateCheckout = function() {

            var self = this;

            var options = {
                date: new Date(),
                mode: 'date'
            };

            datePicker.show(options, function(date){
                var appDate = $filter('date')(date, "d MMM yy");
                self.date.checkout = appDate;
                $scope.$apply();
            });
        }


    })
    .controller('BrowseCtrl', function ($scope, $filter) {
        var self = this;

        L.mapbox.accessToken = 'pk.eyJ1IjoibW91c3RhZyIsImEiOiJDVHlySjU0In0.rzqz_tVGpQKwwcPOC5z4xA';

        var map = L.mapbox.map('map', 'moustag.m1jll6oi', {
            zoomControl: false,
            attributionControl: false
        });

        // Find and store a variable reference to the list of filters.
        var filters = document.getElementById('filters');

        // Wait until the marker layer is loaded in order to build a list of possible
        // types. If you are doing this with another featureLayer, you should change
        // map.featureLayer to the variable you have assigned to your featureLayer.
        map.featureLayer.on('ready', function() {
            console.log('ready')
            // Collect the types of symbols in this layer. you can also just
            // hardcode an array of types if you know what you want to filter on,
            // like var types = ['foo', 'bar'];
            var typesObj = {}, types = [];
            var features = map.featureLayer.getGeoJSON().features;
            for (var i = 0; i < features.length; i++) {
                typesObj[features[i].properties['marker-symbol']] = true;
            }
            for (var k in typesObj) types.push(k);

            var checkboxes = [];

            // Create a filter interface.
            for (var i = 0; i < types.length; i++) {
                // Create an an input checkbox and label inside.
                var item = filters.appendChild(document.createElement('div'));
                var checkbox = item.appendChild(document.createElement('input'));
                var label = item.appendChild(document.createElement('div'));
                checkbox.type = 'checkbox';
                checkbox.id = types[i];
                checkbox.checked = true;

                // create a label to the right of the checkbox with explanatory text
                label.innerHTML = types[i];
                label.setAttribute('for', types[i]);
                label.setAttribute('class', "filter-box");
                // Whenever a person clicks on this checkbox, call the update().
                console.log('add listener')
                checkbox.addEventListener('click', update);
                checkboxes.push(checkbox);
            }

            // This function is called whenever someone clicks on a checkbox and changes
            // the selection of markers to be displayed.
            function update(event) {
                console.log(event);
                var enabled = {};
                // Run through each checkbox and record whether it is checked. If it is,
                // add it to the object of types to display, otherwise do not.
                for (var i = 0; i < checkboxes.length; i++) {
                    console.log(checkboxes[i].checked);
                    if (checkboxes[i].checked) enabled[checkboxes[i].id] = true;
                }
                map.featureLayer.setFilter(function(feature) {
                    // If this symbol is in the list, return true. if not, return false.
                    // The 'in' operator in javascript does exactly that: given a string
                    // or number, it says if that is in a object.
                    // 2 in { 2: true } // true
                    // 2 in { } // false
                    return (feature.properties['marker-symbol'] in enabled);
                });
            }
        });

    });
